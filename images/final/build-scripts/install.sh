#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "apk add --no-cache squid~${BUILD_ARG_SQUID_VERSION}"
echo -e "\033[0;32mInstalled squid:\n$(squid --version)\033[0m"

# Post install
_sh "rm $0"
